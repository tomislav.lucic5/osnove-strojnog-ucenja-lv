import numpy as np
import matplotlib.pylab as plt

#prikazivanje originalne slike
img = plt.imread("road.jpg")
img = img[:,:,0].copy()
print(img)
plt.figure()
plt.imshow(img, cmap="gray")
plt.show()

#a)
plt.figure()
plt.imshow(img, cmap="gray", alpha=0.5)
plt.show()

#b)
quarterImg = img[:, 160:320]
plt.imshow(quarterImg, cmap="gray")
plt.show()

#c)
plt.imshow(np.rot90(img, 3), cmap="gray")
plt.show()

#d)
plt.imshow(np.fliplr(img), cmap="gray")
plt.show()

