import numpy as np
import matplotlib.pyplot as plt


data = np.loadtxt("data.csv", skiprows = 1, delimiter = ',')

#a)
print(len(data))

#b)
plt.scatter(data[:,1], data[:,2], s=0.5)
plt.title("Omjer visine i tezine")
plt.xlabel("Visina")
plt.ylabel("Tezina")
plt.show()

#c)
x = data[::50]
y = data[::50]
plt.scatter(x[:,1], y[:,2])
plt.title("Omjer visine i tezine")
plt.xlabel("Visina")
plt.ylabel("Tezina")
plt.show()

#d)

height = data[:,1]
print(np.max(height))
print(np.min(height))
print(np.average(height))

#e)

man = (data[:,0] == 1)
woman = (data[:,0] == 0)

print(np.max(data[man, 1]))
print(np.min(data[man, 1]))
print(np.average(data[man, 1]))

print(np.max(data[woman, 1]))
print(np.min(data[woman, 1]))
print(np.average(data[woman, 1]))