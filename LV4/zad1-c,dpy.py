from sklearn import datasets
from sklearn . model_selection import train_test_split
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import matplotlib.pyplot as plt
import sklearn . linear_model as lm

data = pd.read_csv('data_C02_emission.csv')

data.drop(["Make", "Model"], axis=1)

input_variables = ['Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)',
                   'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)', 'Engine Size (L)', 'Cylinders']

output_variable = ['CO2 Emissions (g/km)']

x = data[input_variables].to_numpy()
y = data[output_variable].to_numpy()

x_train, x_test, y_train, y_test = train_test_split(
    x, y, test_size=0.2, random_state=1)

scaler = StandardScaler()
plt.figure()
plt.hist(x_train[:, 0], bins=20, color="blue")
plt.show()
x_train_n = scaler.fit_transform(x_train)
x_train_n = scaler.transform(x_train)

plt.figure()
plt.hist(x_train_n[:, 0], bins=20, color="red")
plt.show()

linearModel = lm . LinearRegression()
linearModel . fit(x_train_n, y_train)
print(linearModel.coef_)
