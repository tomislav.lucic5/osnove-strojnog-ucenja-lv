import pandas as pd

data = pd . read_csv ( "data_C02_emission.csv")

#a)
print(len(data))
print(data.info())
print ( data . isnull () . sum () )
print(data.duplicated().sum)

data.drop_duplicates()
data.dropna(inplace=True)
data = data . reset_index ( drop = True )
#data = data.astype("category")

#b)
print(data.sort_values(ascending=False, by='Fuel Consumption City (L/100km)')[['Make', 'Model', 'Fuel Consumption City (L/100km)']].head(3))
print(data.sort_values(ascending=True, by='Fuel Consumption City (L/100km)')[['Make', 'Model', 'Fuel Consumption City (L/100km)']].head(3))

#c)
motorSize = data[(data["Engine Size (L)"] >= 2.5) & (data["Engine Size (L)"] <= 3.5)]
print(len(motorSize))
print(motorSize[['CO2 Emissions (g/km)']].mean())

#d)
audi = data[(data["Make"]=='Audi')]
print(len(audi))
print(audi[(audi["Cylinders"] == 4)][['CO2 Emissions (g/km)']].mean())

#e)
cylinder_counts = data["Cylinders"].value_counts()
print(cylinder_counts)

avgCO2 = data.groupby("Cylinders")["CO2 Emissions (g/km)"].mean()
print(avgCO2)

#f)
dizel = data[(data["Fuel Type"]=='D')]
print(dizel["Fuel Consumption City (L/100km)"].mean())
print(dizel.median())
benzin = data[(data["Fuel Type"]=='X') | (data["Fuel Type"]=='Z')]
print(benzin["Fuel Consumption City (L/100km)"].mean())
print(benzin.median())



#g)
fourC = data[(data["Cylinders"]==4) & (data["Fuel Type"]=='D')]
print(fourC.sort_values(ascending=False, by='Fuel Consumption City (L/100km)')[['Make', 'Model']].head(1))
#h)
manual = data[data["Transmission"].str.contains("M")]
print(len(manual))

#i)
print ( data . corr ( numeric_only = True ) )