songDoc = open('song.txt')
wordCount = {}

for line in songDoc:
    words = line.split()

    for word in words:
        if word in wordCount:
            wordCount[word] += 1
        else:
            wordCount[word] = 1
        
unique_words = [word for word in wordCount if wordCount[word] == 1]    
print (len (unique_words))
print(unique_words)
