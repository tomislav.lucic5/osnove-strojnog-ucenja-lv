import re

filename = "SMSSpamCollection.txt"
with open(filename, "r", encoding="utf-8") as file:
    lines = file.readlines()

ham_count = 0
ham_words = 0
spam_count = 0
spam_words = 0
spam_exclamation_count = 0

for line in lines:
    label, text = line.split("\t")
    words = re.findall(r'\w+', text)
    if label == "ham":
        ham_count += 1
        ham_words += len(words)
    elif label == "spam":
        spam_count += 1
        spam_words += len(words)
        if text[-2] == "!":
            spam_exclamation_count += 1

ham_avg_words = ham_words / ham_count
spam_avg_words = spam_words / spam_count

print("Prosječan broj riječi u ham porukama:", ham_avg_words)
print("Prosječan broj riječi u spam porukama:", spam_avg_words)
print("Broj spam poruka koje završavaju uskličnikom:", spam_exclamation_count)