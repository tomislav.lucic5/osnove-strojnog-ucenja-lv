numbers = []
while True:
    x = input("Unesite broj ili 'done' za kraj programa: ")
    if x.lower() == "done":
        break
    try: 
        number = float(x)
        numbers.append(number)
    except ValueError:
        print("Invalid parameter inserted!")

if len (numbers) == 0:
    print("Lista je prazna")
else:
    n = len (numbers)
    maximum = max (numbers)
    minimum = min (numbers)
    avg = sum(numbers)/n

print(f"Broj vrijednosti u listi: {n}")
print(f"Najveci broj u listi je: {maximum}")
print(f"Najmanji broj u listi je: {minimum}")
print(f"Srednja vrijednost liste je: {avg}")
    