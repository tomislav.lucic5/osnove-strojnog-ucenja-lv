num = None
try:
    num = float(input("Enter a number between 0.0 and 1.0: "))
    if num < 0 or num > 1.0:
        print("You have entered an invalid number")
except ValueError:
    print("You have entered a non-numeric value")
except NameError:
    print("You have entered a non-numeric value")
finally:
    if num is not None and num < 0.6:
        print("F")
    elif num is not None and num >= 0.6 and num < 0.7:
        print("D")
    elif num is not None and num >= 0.7 and num < 0.8:
        print("C")
    elif num is not None and num >= 0.8 and num < 0.9:
        print("B")
    elif num is not None and num >= 0.9 and num <= 1.0:
        print("A")